﻿Module Module1
    Public bytes(1024) As Byte
    Public length As Integer
    Public Fault, Load As Boolean
    Public Start As Boolean
    Public received, total, STATUS As String
    Public Readable As String
    Public First As Boolean
    Public sent As Boolean
    Public Var As String
    Public CoLOS_statusQuery As Byte() = {&HE, &H0, &H0, &H10, &H4, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
    Public CoLOS_Start As Byte() = {&H0, &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
    Public CoLOS_Stop As Byte() = {&H1, &H0, &H0, &H10, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
    Public Print_Status_reply As Byte() = {&H20, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
    Public PEN_Status_reply As Byte() = {&H9, &H0, &H0, &H20, &H1C, &H0, &H0, &H0, &H5C, &H0, &H4F, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H1, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H1, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
    Public Running_reply As Byte() = {&H0, &H0, &H0, &H20, &H8, &H0, &H0, &H0, &H65, &H0, &H74, &H0, &H1, &H0, &H0, &H0, &H1, &H0, &H0, &H0}
    Public Paused_reply As Byte() = {&H0, &H0, &H0, &H20, &H8, &H0, &H0, &H0, &H65, &H0, &H74, &H0, &H0, &H0, &H0, &H0, &H1, &H0, &H0, &H0}
    Public Start_ACK As Byte() = {&H0, &H0, &H0, &H20, &H8, &H0, &H0, &H0, &H0, &H69, &H0, &H63, &H1, &H0, &H0, &H0, &H1, &H0, &H0, &H0}
    Public Stop_ACK As Byte() = {&H0, &H0, &H0, &H20, &H8, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H1, &H0, &H0, &H0}
    Public ACK As Byte() = {&H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
    Public Ping_Request As Byte() = {&H61, &H62, &H63, &H64, &H65, &H66, &H67, &H68, &H69, &H6A, &H6B, &H6C, &H6D, &H6E, &H6F, &H70, &H71, &H72, &H73, &H74, &H75, &H76, &H77, &H61, &H62, &H63, &H64, &H65, &H66, &H67, &H68, &H69}
    Public Job_ACK As Byte() = {&H15, &H0, &H0, &H20, &H8, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
    Public Load_ACK As Byte() = {&H5, &H0, &H0, &H20, &H8, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &H0}
    Public CR_LF As Byte() = {&HA, &HD}
    Public Variable As String
    Public Cut_string As String
    Public line1_IP, line2_IP As String
    Public DJ45_Port As Integer = 5888
    Public DJ45_Header As String = "|cmd|CMD|"
    Public DJ45_Start As String = "startPrint"
    Public DJ45_Stop As String = "stopPrint"
    Public DJ45_UpdateVar As String = "setTextValue "
    Public DJ45_Status As String = "queryPrintStatus"
    Public DJ45_Select_Job As String = "|cmd|CMD|openFile"
    Public DJ45_Command As String
End Module
