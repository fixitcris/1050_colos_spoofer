﻿Imports System.ComponentModel
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Threading
Imports System.Windows.Threading

Class MainWindow
    Dim WithEvents Tim As New DispatcherTimer
    Dim nIcon As New NotifyIcon
    Dim menu As New ContextMenu
    Dim WithEvents Closer As New MenuItem
    Dim WithEvents Tray As New BackgroundWorker
    Dim WithEvents CoLOS_Listener As New BackgroundWorker
    Dim WithEvents CoLOS_Listener2 As New BackgroundWorker

    Private Sub MainWindow_Initialized(sender As Object, e As EventArgs) Handles Me.Initialized
        Tim.Interval = New TimeSpan(0, 0, 30)
        line1_IP = My.Settings.Line1_DJ45_IP
        line2_IP = My.Settings.Line2_DJ45_IP
        Tray.RunWorkerAsync()
        Fault = False
        WindowState = System.Windows.WindowState.Minimized
        CoLOS_Listener.RunWorkerAsync()
        CoLOS_Listener2.RunWorkerAsync()
    End Sub
    Private Sub Closer_Click() Handles Closer.Click
        Close()
    End Sub

    Public Shared Sub TO_DJ45(ip As String)
        DJ45_Port = My.Settings.DJ45_PORT
        Using DJ45 As New TcpClient
            Try
                DJ45.Connect(ip, DJ45_Port)
                Dim DJ45_WRITER As New StreamWriter(DJ45.GetStream)
                Dim DJ45_Reader As New StreamReader(DJ45.GetStream)
                While DJ45.Connected = True
                    Console.WriteLine("Sending to DJ45: " & Var & Environment.NewLine)
                    DJ45_WRITER.Write(Var & Environment.NewLine)
                    DJ45_WRITER.Flush()
                    DJ45_WRITER.Close()
                    DJ45.Close()
                    Fault = False
                End While
            Catch ex As Exception
                MessageBox.Show("DJ45 Comms fault: " & ex.Message)
                Fault = True
                DJ45.Close()
            End Try
        End Using
    End Sub

    Private Sub Tray_DoWork(sender As Object, e As DoWorkEventArgs) Handles Tray.DoWork
        nIcon.Icon = New Icon("C:\program files\Mitech\1050 Spoofer\Mitech_Disk.ico")
        nIcon.Visible = True
        nIcon.Text = "1050 to DJ45 translator"
        Dim menu As New ContextMenu
        menu.MenuItems.Add(Closer)
        Closer.Text = "CLOSE"
        nIcon.ContextMenu = menu
    End Sub

    Private Sub CoLOS_Listener_DoWork(sender As Object, e As DoWorkEventArgs) Handles CoLOS_Listener.DoWork
        Dim server As TcpListener
        Dim received As String
        Dim tmp As String
        Dim tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10, tmp11, tmp12, tmp13, tmp14, tmp15 As String
        server = Nothing
        Dim i As Integer
        If Fault = False Then
            Try
                Dim port As Integer = 700
                Dim localAddr As IPAddress = IPAddress.Any
                server = New TcpListener(localAddr, port)
                server.Start()
                Console.WriteLine("Line 1 Server Started")
                Console.WriteLine("Line 1 Waiting for a connection from CoLOS...")
                While True
                    Dim client As TcpClient = server.AcceptTcpClient()
                    Console.WriteLine("Line 1 Connected!")
                    received = Nothing
                    While True
                        bytes.DefaultIfEmpty
                        Dim stream As NetworkStream = client.GetStream()
                        i = stream.Read(bytes, 0, bytes.Length)
                        Readable = ByteArrayToString(bytes)
                        received = System.Text.Encoding.UTF8.GetString(bytes)
                        Try
                            If bytes(0) = 6 And bytes(3) = 16 Then
                                stream.Write(Print_Status_reply, 0, Print_Status_reply.Length)
                                stream.Write(Running_reply, 0, Running_reply.Length)
                            ElseIf bytes(0) = 14 Then
                                If Start = False Then
                                    stream.Write(PEN_Status_reply, 0, PEN_Status_reply.Length)
                                    stream.Write(Paused_reply, 0, Paused_reply.Length)
                                Else
                                    stream.Write(PEN_Status_reply, 0, PEN_Status_reply.Length)
                                    stream.Write(Running_reply, 0, Running_reply.Length)
                                End If
                            ElseIf bytes(3) = 16 And bytes(0) = 0 Then
                                stream.Write(Start_ACK, 0, Start_ACK.Length)
                            ElseIf bytes(3) = 16 And bytes(0) = 1 Then
                                stream.Write(Stop_ACK, 0, Stop_ACK.Length)
                                Var = DJ45_Header + DJ45_Stop
                                TO_DJ45(line1_IP)
                                Load = False
                            ElseIf bytes(3) = 16 And bytes(0) = 21 Then
                                If bytes(28) = &H2E Then
                                    tmp = Strings.Left(received, 27)
                                    tmp1 = Right(tmp, 7)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 5)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 3)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp13 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp13
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                ElseIf bytes(30) = &H2E Then
                                    tmp = Strings.Left(received, 29)
                                    tmp1 = Right(tmp, 9)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 7)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 5)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp7 = Right(tmp, 3)
                                    tmp8 = Strings.Left(tmp7, 1)
                                    tmp13 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp8 & tmp13
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                ElseIf bytes(32) = &H2E Then
                                    tmp = Strings.Left(received, 31)
                                    tmp1 = Right(tmp, 11)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 9)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 7)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp7 = Right(tmp, 5)
                                    tmp8 = Strings.Left(tmp7, 1)
                                    tmp9 = Right(tmp, 3)
                                    tmp10 = Strings.Left(tmp9, 1)
                                    tmp13 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp8 & tmp10 & tmp13
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                ElseIf bytes(34) = &H2E Then
                                    tmp = Strings.Left(received, 33)
                                    tmp1 = Right(tmp, 13)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 11)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 9)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp7 = Right(tmp, 7)
                                    tmp8 = Strings.Left(tmp7, 1)
                                    tmp9 = Right(tmp, 5)
                                    tmp10 = Strings.Left(tmp9, 1)
                                    tmp11 = Right(tmp, 3)
                                    tmp12 = Strings.Left(tmp11, 1)
                                    tmp13 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp8 & tmp10 & tmp12 & tmp13
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                ElseIf bytes(34) = &H2E Then
                                    tmp = Strings.Left(received, 35)
                                    tmp1 = Right(tmp, 15)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 13)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 11)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp7 = Right(tmp, 9)
                                    tmp8 = Strings.Left(tmp7, 1)
                                    tmp9 = Right(tmp, 7)
                                    tmp10 = Strings.Left(tmp9, 1)
                                    tmp11 = Right(tmp, 5)
                                    tmp12 = Strings.Left(tmp11, 1)
                                    tmp13 = Right(tmp, 3)
                                    tmp14 = Strings.Left(tmp13, 1)
                                    tmp15 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp8 & tmp10 & tmp12 & tmp14 & tmp15
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                End If
                                stream.Write(Job_ACK, 0, Job_ACK.Length)
                                Var = DJ45_Header & DJ45_UpdateVar & total
                                TO_DJ45(line1_IP)
                                Thread.Sleep(2000)
                                Var = DJ45_Header + DJ45_Start
                                TO_DJ45(line1_IP)
                                Start = True
                            ElseIf bytes(3) = 16 And bytes(0) = 10 Then
                                stream.Write(Load_ACK, 0, Load_ACK.Length)
                            End If
                            First = True
                        Catch ex As Exception
                            MessageBox.Show("CoLOS Comms fault: " & ex.Message)
                            Exit While
                        End Try
                    End While
                    client.Close()
                End While
            Catch ex As SocketException
                Console.WriteLine("SocketException: {0}", ex)
                MessageBox.Show("CoLOS Comms fault: " & ex.Message)
            Finally
                server.Stop()
            End Try
        End If
    End Sub

    Private Sub CoLOS_Listener2_DoWork(sender As Object, e As DoWorkEventArgs) Handles CoLOS_Listener2.DoWork
        Dim server As TcpListener
        Dim received As String
        Dim tmp As String
        Dim tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10, tmp11, tmp12, tmp13, tmp14, tmp15 As String
        server = Nothing
        Dim i As Integer
        If Fault = False Then
            Try
                Dim port As Integer = 701
                Dim localAddr As IPAddress = IPAddress.Any
                server = New TcpListener(localAddr, port)
                server.Start()
                Console.WriteLine("Line 2 Server Started")
                Console.WriteLine("Line 2 Waiting for a connection from CoLOS...")
                While True
                    Dim client As TcpClient = server.AcceptTcpClient()
                    Console.WriteLine("Line 2 Connected!")
                    received = Nothing
                    While True
                        bytes.DefaultIfEmpty
                        Dim stream As NetworkStream = client.GetStream()
                        i = stream.Read(bytes, 0, bytes.Length)
                        Readable = ByteArrayToString(bytes)
                        received = System.Text.Encoding.UTF8.GetString(bytes)
                        Try
                            If bytes(0) = 6 And bytes(3) = 16 Then
                                stream.Write(Print_Status_reply, 0, Print_Status_reply.Length)
                                stream.Write(Running_reply, 0, Running_reply.Length)
                            ElseIf bytes(0) = 14 Then
                                If Start = False Then
                                    stream.Write(PEN_Status_reply, 0, PEN_Status_reply.Length)
                                    stream.Write(Paused_reply, 0, Paused_reply.Length)
                                Else
                                    stream.Write(PEN_Status_reply, 0, PEN_Status_reply.Length)
                                    stream.Write(Running_reply, 0, Running_reply.Length)
                                End If
                            ElseIf bytes(3) = 16 And bytes(0) = 0 Then
                                stream.Write(Start_ACK, 0, Start_ACK.Length)
                            ElseIf bytes(3) = 16 And bytes(0) = 1 Then
                                stream.Write(Stop_ACK, 0, Stop_ACK.Length)
                                Var = DJ45_Header + DJ45_Stop
                                TO_DJ45(line2_IP)
                                Load = False
                            ElseIf bytes(3) = 16 And bytes(0) = 21 Then
                                If bytes(28) = &H2E Then
                                    tmp = Strings.Left(received, 27)
                                    tmp1 = Right(tmp, 7)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 5)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 3)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp13 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp13
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                ElseIf bytes(30) = &H2E Then
                                    tmp = Strings.Left(received, 29)
                                    tmp1 = Right(tmp, 9)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 7)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 5)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp7 = Right(tmp, 3)
                                    tmp8 = Strings.Left(tmp7, 1)
                                    tmp13 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp8 & tmp13
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                ElseIf bytes(32) = &H2E Then
                                    tmp = Strings.Left(received, 31)
                                    tmp1 = Right(tmp, 11)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 9)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 7)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp7 = Right(tmp, 5)
                                    tmp8 = Strings.Left(tmp7, 1)
                                    tmp9 = Right(tmp, 3)
                                    tmp10 = Strings.Left(tmp9, 1)
                                    tmp13 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp8 & tmp10 & tmp13
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                ElseIf bytes(34) = &H2E Then
                                    tmp = Strings.Left(received, 33)
                                    tmp1 = Right(tmp, 13)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 11)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 9)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp7 = Right(tmp, 7)
                                    tmp8 = Strings.Left(tmp7, 1)
                                    tmp9 = Right(tmp, 5)
                                    tmp10 = Strings.Left(tmp9, 1)
                                    tmp11 = Right(tmp, 3)
                                    tmp12 = Strings.Left(tmp11, 1)
                                    tmp13 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp8 & tmp10 & tmp12 & tmp13
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                ElseIf bytes(34) = &H2E Then
                                    tmp = Strings.Left(received, 35)
                                    tmp1 = Right(tmp, 15)
                                    tmp2 = Strings.Left(tmp1, 1)
                                    tmp3 = Right(tmp, 13)
                                    tmp4 = Strings.Left(tmp3, 1)
                                    tmp5 = Right(tmp, 11)
                                    tmp6 = Strings.Left(tmp5, 1)
                                    tmp7 = Right(tmp, 9)
                                    tmp8 = Strings.Left(tmp7, 1)
                                    tmp9 = Right(tmp, 7)
                                    tmp10 = Strings.Left(tmp9, 1)
                                    tmp11 = Right(tmp, 5)
                                    tmp12 = Strings.Left(tmp11, 1)
                                    tmp13 = Right(tmp, 3)
                                    tmp14 = Strings.Left(tmp13, 1)
                                    tmp15 = Right(tmp, 1)
                                    total = tmp2 & tmp4 & tmp6 & tmp8 & tmp10 & tmp12 & tmp14 & tmp15
                                    Var = DJ45_Header & DJ45_UpdateVar & total
                                End If
                                stream.Write(Job_ACK, 0, Job_ACK.Length)
                                Var = DJ45_Header & DJ45_UpdateVar & total
                                TO_DJ45(line2_IP)
                                Thread.Sleep(2000)
                                Var = DJ45_Header + DJ45_Start
                                TO_DJ45(line2_IP)
                                Start = True
                            ElseIf bytes(3) = 16 And bytes(0) = 10 Then
                                stream.Write(Load_ACK, 0, Load_ACK.Length)
                            End If
                            First = True
                        Catch ex As Exception
                            MessageBox.Show("CoLOS Comms fault: " & ex.Message)
                            Exit While
                        End Try
                    End While
                    client.Close()
                End While
            Catch ex As SocketException
                Console.WriteLine("SocketException: {0}", ex)
                MessageBox.Show("CoLOS Comms fault: " & ex.Message)
            Finally
                server.Stop()
            End Try
        End If
    End Sub

    Private Shared Function ByteArrayToString(ByVal arrInput() As Byte) As String
        Dim i As Integer
        Dim sOutput As New StringBuilder(arrInput.Length)
        For i = 0 To arrInput.Length - 1
            sOutput.Append(arrInput(i).ToString("X2"))
        Next
        Return sOutput.ToString()
    End Function

    Private Sub Tim_Tick(sender As Object, e As EventArgs) Handles Tim.Tick
        Load = False
    End Sub
End Class
